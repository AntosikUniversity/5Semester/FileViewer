﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Data;
using FileViewer.Interfaces;
using FileViewer.Models;
using Directory = FileViewer.Models.Directory;
using File = FileViewer.Models.File;
using IOPath = System.IO.Path;

namespace FileViewer.ViewModels
{
    public class DirectoryViewModel : ViewModelBase, IDirectoryViewModel
    {
        private readonly object _lockObject = new object();

        private FsWrapper _fileTree;
        private string _name;
        private string _path;

        public DirectoryViewModel(string dirpath)
        {
            Path = dirpath;
            Name = IOPath.GetFileName(dirpath);

            Watcher = new FileSystemWatcher
            {
                Path = dirpath,
                IncludeSubdirectories = true
            };
            Watcher.Created += OnChanged;
            Watcher.Deleted += OnChanged;
            Watcher.Renamed += OnRenamed;
            Watcher.EnableRaisingEvents = true;

            Tree = new ObservableCollection<INodeWrapper>();
            BindingOperations.EnableCollectionSynchronization(Tree, _lockObject);

            InitTree();
        }

        public ObservableCollection<INodeWrapper> Tree { get; }
        public string Name
        {
            get => _name;
            set => Change(ref _name, value);
        }

        public string Path
        {
            get => _path;
            set => Change(ref _path, value);
        }

        private FileSystemWatcher Watcher { get; }

        public void OnChanged(object source, FileSystemEventArgs e)
        {
            if (_fileTree == null) return;

            var parentPath = IOPath.GetDirectoryName(e.FullPath);

            var node = GetNode(e.FullPath) as IDirectory;
            if (node == null) return;

            Task.Run(() =>
            {
                node.Directories.Clear();
                node.Files.Clear();
                BuildTree(ref node, parentPath);
            });
        }

        public IDirectory BuildTree(ref IDirectory directory, string path)
        {
            if (directory == null)
                directory = new Directory
                {
                    Name = path.Split('\\').Last(),
                    Path = path
                };
            try
            {
                object lockObjDirs = new object(), lockObjFiles = new object();
                BindingOperations.EnableCollectionSynchronization(directory.Directories, lockObjDirs);
                BindingOperations.EnableCollectionSynchronization(directory.Files, lockObjFiles);

                directory.Files.Clear();
                foreach (var filePath in System.IO.Directory.GetFiles(path))
                {
                    var fileName = filePath.Split('\\').Last();
                    directory.Files.Add(
                        new File
                        {
                            Name = fileName,
                            Path = filePath
                        });
                }
                foreach (var dirPath in System.IO.Directory.GetDirectories(path))
                {
                    var dirName = dirPath.Split('\\').Last();
                    directory.Directories.Add(
                        new Directory
                        {
                            Name = dirName,
                            Path = dirPath
                        });
                    var dir = directory.Directories.Last();
                    dir.Files.Add(new File{Name="loading...", Path = "..."});
                    Task.Run(() => BuildTree(ref dir, dirPath));
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine($"ERROR! {e.Message}");
            }
            return directory;
        }

        public void OnRenamed(object source, RenamedEventArgs e)
        {
            if (_fileTree == null) return;

            var parentPath = IOPath.GetDirectoryName(e.OldFullPath);
            var node = GetNode(e.OldFullPath) as IDirectory;
            if (node == null) return;

            Task.Run(() =>
            {
                node.Directories.Clear();
                node.Files.Clear();
                BuildTree(ref node, parentPath);
            });
        }

        private void InitTree()
        {
            var dd = new Directory
            {
                Name = Name,
                Path = Path
            } as IDirectory;

            Task.Run(() =>
            {
                Tree.Clear();
                _fileTree = new FsWrapper(BuildTree(ref dd, Path));
                Tree.Add(_fileTree);
            });
        }

        private ref INode GetNode(string path)
        {
            uint pointer = 0;
            var relativePath = path.Replace($"{Path}\\", "");
            var namesArr = relativePath.Split('\\');
            INodeWrapper dir = _fileTree;

            while (namesArr.Length > 1)
            {
                var dirname = namesArr[pointer];
                if (dirname == dir.Name) break;
                try
                {
                    dir = dir.Children.First(item => item.Name == dirname);
                }
                catch
                {
                    // ignored
                }
            }

            return ref dir.GetNode();
        }
    }
}