﻿using System.IO;
using System.Threading.Tasks;
using FileViewer.Interfaces;
using IOPath = System.IO.Path;

namespace FileViewer.ViewModels
{
    public class FileViewModel : ViewModelBase, INode
    {
        private string _name;

        private long _openProgressMax;
        private long _openProgressValue;
        private string _path;

        public FileViewModel(string filepath)
        {
            Path = filepath;
            Name = IOPath.GetFileName(filepath);

            Content = "";
            OpenProgressValue = 0;
            OpenProgressMax = 0;
        }

        public long OpenProgressValue
        {
            get => _openProgressValue;
            set => Change(ref _openProgressValue, value);
        }

        public long OpenProgressMax
        {
            get => _openProgressMax;
            set => Change(ref _openProgressMax, value);
        }

        public string Content { get; private set; }

        public string Name
        {
            get => _name;
            set => Change(ref _name, value);
        }

        public string Path
        {
            get => _path;
            set => Change(ref _path, value);
        }

        public async void OpenFile()
        {
            OpenProgressMax = new FileInfo(Path).Length;

            Content = await Task.Factory.StartNew(
                LoadFileContentInHex,
                TaskCreationOptions.LongRunning
            );
            OnPropertyChanged(nameof(Content));
        }

        public string LoadFileContentInHex()
        {
            using (var fileStream = File.Open(Path, FileMode.Open, FileAccess.Read))
            {
                var result = "";
                int b;

                while ((b = fileStream.ReadByte()) > 0)
                {
                    result += $"{b:X2}";
                    OpenProgressValue++;
                }

                return result;
            }
        }
    }
}