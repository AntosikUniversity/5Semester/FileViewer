﻿using FileViewer.Interfaces;

namespace FileViewer.Models
{
    public class File : INode
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }
}