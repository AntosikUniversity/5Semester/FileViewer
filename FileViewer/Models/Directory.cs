﻿using System.Collections.ObjectModel;
using FileViewer.Interfaces;

namespace FileViewer.Models
{
    public class Directory : IDirectory
    {
        public Directory()
        {
            Directories = new ObservableCollection<IDirectory>();
            Files = new ObservableCollection<INode>();
        }

        public string Name { get; set; }
        public string Path { get; set; }

        public ObservableCollection<INode> Files { get; set; }
        public ObservableCollection<IDirectory> Directories { get; set; }
    }
}