﻿using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace FileViewer.Helpers
{
    public class CommandFactory
    {
        public CommandFactory()
        {
            _commands = new Dictionary<string, ICommand>();
        }

        private Dictionary<string, ICommand> _commands;

        public CommandFactory AddCommand(string name, Action action, Func<bool> func = null)
        {
            Commands[name] = new Command(action, func);
            return this;
        }
        public CommandFactory AddCommand(string name, Action<object> action, Func<bool> func = null)
        {
            Commands[name] = new Command(action, func);
            return this;
        }
        public CommandFactory RemoveCommand(string name)
        {
            Commands.Remove(name);
            return this;
        }

        public Dictionary<string, ICommand> Commands => _commands;
    }
}