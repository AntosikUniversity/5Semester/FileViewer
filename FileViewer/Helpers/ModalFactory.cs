﻿using FileViewer.ViewModels;
using FileViewer.Windows;

namespace FileViewer.Helpers
{
    public class ModalFactory
    {
        public void Create(ModalTypesEnum type, object target)
        {
            var modal = new FileModal
            {
                DataContext = new ModalView(type, target)
            };
            modal.ShowDialog();
        }
    }
}