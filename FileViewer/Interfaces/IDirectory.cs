﻿using System.Collections.ObjectModel;

namespace FileViewer.Interfaces
{
    public interface IDirectory : INode
    {
        ObservableCollection<INode> Files { get; set; }
        ObservableCollection<IDirectory> Directories { get; set; }
    }
}