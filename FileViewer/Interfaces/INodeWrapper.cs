﻿using System.Collections.Generic;

namespace FileViewer.Interfaces
{
    public interface INodeWrapper
    {
        INode Item { get; set; }
        string Name { get; }
        string Path { get; }
        List<INodeWrapper> Children { get; }
        ref INode GetNode();
    }
}