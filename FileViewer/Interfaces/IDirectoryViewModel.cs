﻿using System.Collections.ObjectModel;
using System.IO;

namespace FileViewer.Interfaces
{
    public interface IDirectoryViewModel : INode
    {
        ObservableCollection<INodeWrapper> Tree { get; }

        void OnChanged(object source, FileSystemEventArgs e);
        IDirectory BuildTree(ref IDirectory directory, string path);
    }
}