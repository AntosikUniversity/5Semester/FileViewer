﻿using System.Windows.Input;

namespace FileViewer.Interfaces
{
    public interface IModal
    {
        string Name { get; set; }
        string Value { get; set; }
        string Description { get; set; }
        ICommand OnOk { get; set; }
    }
}